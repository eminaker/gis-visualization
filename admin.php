<?php
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    include_once 'includes/database.php';

    sec_session_start();

    if (login_check($mysqli) == true) {
        $logged = 'in';
    } else {
        $logged = 'out';
    }
    if($logged == 'out'){
        header('Location: login.php');
	//echo 'LOG IN!';
    }
    /*
     * Accepts a Dataset Name from a form element coupled with a multiple
     * file element (up to 1GB, 1000 unique files) and uploads the files
     * to a folder named $dataSetName within $dirP.
     * Then adds the new dataset to the database, using an A_I ID.
     * Then adds each file to the database as a map, using an A_I ID.
     */
    $mToggle=FALSE;
    if($_POST['submit']=="Upload"){
        $dirP= "/var/www/gisVisualization/UserData/";
        $dataSetName = $_POST['datasetName'];
        $count = 0;
        $countB = 0;
        $ran = 1;
        $dataSetName =  preg_replace('~\x{00a0}~','',$dataSetName);
        $dirFP = $dirP . $dataSetName."/";
        $mkCMD="mkdir $dirFP";
        exec($mkCMD);
        foreach ($_FILES['uploadFile']['name'] as $f => $name) {
            if(move_uploaded_file($_FILES["uploadFile"]["tmp_name"][$f], $dirFP.$name))
                $count++;
            else
                $countB++;
        }
        $files = scandir("$dirFP");


        $queryD="INSERT INTO datasets VALUES('$dataSetName','$dataSetName','');";
        $resultD=mysql_query($queryD) or die(mysql_error());
        $queryDID="SELECT id FROM datasets WHERE name LIKE '$dataSetName';";
        $resultDID=mysql_query($queryDID) or die(mysql_error());
        $dsID=mysql_fetch_object($resultDID);
        $mBool = false;
        $yBool = false;
        foreach($files as $file){
            explode("_",$file);
            list($ph, $ph2, $monthTmp, $yearTmpR) = explode("_",$file);
            if($monthTmp){
                $month = $monthTmp;
                $mBool = true;
            }
            list($yearTmp, $extension) = explode(".",$yearTmpR);
            if($yearTmp){
                $year = $yearTmp;
                $yBool=true;
            }
            if($mBool==true && $yBool==true){
                $dateM=$year."-".$month."-01";
                $filePath="UserData/$dataSetName/$file";
                $queryM="INSERT INTO maps VALUES('$dateM','$file','$filePath','$dsID->id','');";
                $resultM=mysql_query($queryM) or die(mysql_error());
            }
        }
    }
    /*
     * Deletes user selected dataset from database and then from filesystem.
     */
    if($_POST['submit']=="Delete"){
        $con = mysql_connect('localhost', 'eminaker', 'Eric42dm@');
        if (!$con){
            die('Could not connect: ' . mysql_error());
        }
        $UDIDP=$_POST['id'];
        //echo $UDIDP;
        mysql_select_db('droughtDataLF') or die('Could not select database');
        $dirP= "/var/www/gisVisualization/UserData/";
        $dBool =false;
        $dmBool=false;
        $ddBool=false;
        //$dataSetName = $_POST['datasetName'];
        //$queryI="SELECT id FROM datasets WHERE name LIKE '$dataSetName'";
        //$resultDID=mysql_query($queryI) or die(mysql_error());
        //$DID=mysql_fetch_object($resultDID);
        //$UDID=$DID->id;
        $queryD="DELETE FROM maps WHERE dataset LIKE '$UDIDP'";
        $resultD=mysql_query($queryD) or die(mysql_error());
        if($resultD)
            $dmBool=true;
        $queryDD="DELETE FROM datasets WHERE id LIKE '$UDIDP'";
        $resultDD=mysql_query($queryDD) or die(mysql_error());
        if($resultDD)
            $ddBool=true;
        if($dmBool && $ddBool){
            $rmCMD="rm -rf $dirP$dataSetName";
            exec($rmCMD);$dBool = true;

            $dBool = true;

        }
    }

    if($_POST['submit']=="Manage"){
        $mToggle=TRUE;

    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Visualizing Drought :: LSSU</title>
    <meta charset="utf-8">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="fonts.css">
    <script src="js/jquery-1.11.2.js"></script>
    <script src="paginate.js"></script>
    <?php
        if($dBool==true){
            echo "<style>

                #manageCon h3{
                -webkit-animation-name: animation;
                -webkit-animation-duration: 4s;
                -webkit-animation-timing-function: ease-in-out;
                -webkit-animation-iteration-count: 1;
                -webkit-animation-play-state: running; }
                animation-name: animation;
                animation-duration: 4s;
                animation-timing-function: ease-in-out;
                animation-iteration-count: 1;
                animation-play-state: running;
                -moz-animation-name: animation;
                -moz-animation-duration: 4s;
                -moz-animation-timing-function: ease-in-out;
                -moz-animation-iteration-count: 1;
                -moz-animation-play-state: running;
                background-color: #87C540;}
                </style>";
            echo "<style>
                 .manageHead:before{
                     content:\"Dataset Deleted\";
                 }
                  </style>";
        }
        if($countB == 0 && $ran == 1){
            echo "<style>
                #uploadCon h3{
                -webkit-animation-name: animation;
                -webkit-animation-duration: 4s;
                -webkit-animation-timing-function: ease-in-out;
                -webkit-animation-iteration-count: 1;
                -webkit-animation-play-state: running; }
                animation-name: animation;
                animation-duration: 4s;
                animation-timing-function: ease-in-out;
                animation-iteration-count: 1;
                animation-play-state: running;
                -moz-animation-name: animation;
                -moz-animation-duration: 4s;
                -moz-animation-timing-function: ease-in-out;
                -moz-animation-iteration-count: 1;
                -moz-animation-play-state: running;
$con = mysql_connect('localhost', 'eminaker', 'devPass');
                background-color: #87C540;
                </style>";
            echo "  <style>
                    .uploadHead:before{
                        content:\"Dataset Uploaded\";
                    }
                    </style>";
        }
        elseif($countB != 0 && $ran ==1){
            echo"<style>#uploadCon h3{ background-color:#CC4B4B; }</style>";
        }
    ?>
</head>
<body onLoad="paginate();">
<div class="wrapper">
    <div class="header">
        <div class="logo">
            <img style="width:50px;" class="respImg" src="images/secondary-block.jpg">
        </div> <!-- END OF LOGO -->
        <div class="menu">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div> <!-- END OF MENU -->
        <div class="clear"></div>
    </div> <!-- END OF HEADER -->
    <div class="formCon" id="uploadCon">
        <h3 class="uploadHead"></h3>
        <form name="uploadForm" method="post" enctype="multipart/form-data">
            <p>Dataset Name <input type="text" name="datasetName" id="datasetName">
            <input type="file" multiple="multiple" name="uploadFile[]" id="uploadFile">
            <input type="submit" name="submit" value="Upload"></p>
        </form>
    </div> <!-- END OF FORMCON for upload -->
    <!-- <div class="formCon" id="deleteCon">
        <h3>Delete Dataset</h3>
        <form name="deleteForm" method="post">
            <p>Dataset Name <input type="text" name="datasetName" id="datasetName">
            <input type="submit" name="submit" value="Delete"></p>
        </form>
    </div> --> <!-- END OF FORMCON for delete -->
    <div class="formCon" id="manageCon">
        <a href="admin.php"><h3 class="manageHead"></h3></a>
            <!-- <form name="deleteFormTab" method="post"> -->
                <?php
                    $con = mysql_connect('localhost', 'eminaker', 'Eric42dm@') or die(mysql_error());
                    mysql_select_db('droughtDataLF') or die('Could not select database');
                    if($mToggle==FALSE){
                        echo "<table id=\"manageTab\" border=0 cellspacing=0 cellpadding=0>";
                        echo "<tr><th>Name</th><th>Description</th><th>ID</th><th></th><th></th></tr>";
                        $querySDS="SELECT * FROM datasets";
                        $queryDS=mysql_query($querySDS);
                        //$resultsDS=mysql_fetch_object($queryDS);
                        while ($line = mysql_fetch_array($queryDS, MYSQL_ASSOC)){
                            echo "<tr>";
                            echo "<td>".$line['name']."</td>";
                            echo "<td>".$line['description']."</td>";
                            echo "<td>".$line['id']."</td>";
                            echo "<form name=\"deleteFormTab\" method=\"post\">";
                            echo "<td><input type=\"hidden\" name=\"id\" value=\"".$line['id']."\"><input type=\"submit\" name=\"submit\" value=\"Delete\"></td>";
                            echo "<td><input type=\"hidden\" name=\"id\" value=\"".$line['id']."\"><input type=\"submit\" name=\"submit\" value=\"Manage\"></td>";
                            echo "</form>";
                            echo "</tr>";
                            //echo "THIS IS THE END OF THE ROW";
                        }
                        echo "</table>";
                        //echo "AND TABLE END";
                    }
                    if($mToggle==TRUE){
                        //echo $_POST['id'];
                        echo "<table id=\"manageMapTab\" class=\"paginated\" border=0 cellspacing=0 cellpadding=0 width=\"100%\">";
                        echo "<tr><thead><th scope=\"col\">ID</th><th scope=\"col\">Date</th><th scope=\"col\">Description</th><th scope=\"col\">Path</th><th scope=\"col\">Dataset</th></thead></tr>";
                        $count = 0;
                        $querySMD="SELECT * FROM maps WHERE dataset LIKE '".$_POST['id']."'";
                        $resultsMD=mysql_query($querySMD);
                        while ($line = mysql_fetch_array($resultsMD, MYSQL_ASSOC)){
                            echo "<tr>";
                            echo "<td>".$line['id']."</td>";
                            echo "<td>".$line['date']."</td>";
                            echo "<td>".$line['description']."</td>";
                            echo "<td>".$line['mapData']."</td>";
                            echo "<td>".$line['dataset']."</td>";
                            echo "</tr>";
                        }
                        echo "</table>";
                    }
                ?>
            <!--</form>-->
        <!-- Table to hold datasets, links to displays in same div in list -->
    </div> <!-- END OF FORMCON for manage -->
    <div class="push clear"></div>
</div> <!-- END OF WRAPPER -->
    <div class="footer clear">
        <ul>
            <li><a href="includes/logout.php">Logout</a></li>
        </ul>
    </div> <!-- END OF FOOTER -->
</body>
