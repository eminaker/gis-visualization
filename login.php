<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';


?>
<html>
<head>
    <title>Visualizing Drought :: LSSU</title>
    <meta charset="utf-8">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="fonts.css">
    <script src="js/jquery-1.11.2.js"></script>
    <script src="forms.js"></script>
    <script src="sha512.js"></script>
    <script type="text/javascript">
        $(function(){
            $('#close').click(function(){
                $('#infoBox').slideUp();
            });
        });
    </script>
</head>
<body> 
<div class="wrapper">
    <div class="header">
        <div class="logo">
            <img style="width:50px;" class="respImg" src="images/secondary-block.jpg">
        </div> <!-- END OF LOGO -->
        <div class="menu">
            <ul>       
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </div> <!-- END OF MENU -->
        <div class="clear"></div>
    </div> <!-- END OF HEADER -->
    <div class="loginCon">
        <div class="login">
            <h2>Please Login</h2>
            <form class="loginForm" method="post" action="includes/process_login.php" name="login_form">
                <p>Email:<span style="margin-right:30px;"></span> <input type="text" name="email" id="email"></p>
                <p>Password: <input type="password" name="password" id="password"></p>
                <input type="submit" value="Login" id="loginSubmit" onclick="formhash(this.form, this.form.password)";>
            </form>
        </div> <!-- END OF LOGIN -->
    </div> <!-- END OF LOGINCON -->
</div> <!-- END OF WRAPPER -->
</body>
</html>
