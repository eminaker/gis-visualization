<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';


?>
<html>
<head>
    <title>Visualizing Drought :: LSSU</title>
    <meta charset="utf-8">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts.css">
    <script src="js/jquery-3.0.0.min.js"></script>
    <script src="forms.js"></script>
    <script src="sha512.js"></script>
    <script type="text/javascript">
        $(function(){
            $('#close').click(function(){
                $('#infoBox').slideUp();
            });
        });
    </script>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <div class="logo">
            <img style="width:50px;" class="respImg" src="images/secondary-block.jpg">
        </div> <!-- END OF LOGO -->
        <div class="menu">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </div> <!-- END OF MENU -->
        <div class="clear"></div>
    </div> <!-- END OF HEADER -->
    <div class="contentCon">
        <p>For more information regarding this application, you may contact the developer at <a href="mailto:eminaker@lssu.edu">eminaker@lssu.edu</a>.</p>
        <p>For more information about the data involved, drought patterns, water management, or Lake Superior State University's School of Physical Sciences and geology programs, please contact Dr. Munoz-Hernandez at <a href="mailto:amunozhernandez@lssu.edu">amunozhernandez@lssu.edu</a>.</p>
        <p>For more information about Lake Superior State University's School of Mathematics and Computer Science, computer science programs, computer networking programs, and web development programs please contact Dr. Christopher Smith at <a href="mailto:csmith16@lssu.edu">csmith16@lssu.edu</a></p>
        <p>If you're interested in touring or attending Lake Superior State Univeristy, you can either <a href="http://www.lssu.edu/admissions/">visit their admissions website</a>.
    </div> <!-- END OF CONTENTCON -->
</div> <!-- END OF WRAPPER -->
</body>
</html>
