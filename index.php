<?php
    require_once("includes/map_func.php");
    require_once("includes/database.php");

    $uid = getUID();

    $fetchR=$_GET["idR"];
    if(!$fetchR)
        $fetchR=14;

    $fetchL=$_GET["idL"];
    if(!$fetchL)
        $fetchL=14;

    $fetchIDR=$_GET["mapIDR"];
    if(!$fetchIDR)
        $fetchIDR=190;

    $fetchIDL=$_GET["mapIDL"];
    if(!$fetchIDL)
        $fetchIDL=194;
?>

<!DOCTYPE html>
<html>
<head>
    <title>Visualizing Drought</title>
    <meta charset="utf-8">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts.css">
    <script src="js/jquery-3.0.0.min.js"></script>
    <script type="text/javascript">
        /*
        * If a new dataset is selected - refresh select drop down.
        * If new map selected from drop down - refresh map render.
        * Use two separate functions, and then the calls will send the approriate
        * data and whether it's right or left.

        $.ajax({
          method: "POST",
          url: "includes/map_func.php",
          data: { mapL: "John", mapR: "Boston", mapIDL: "", mapIDR: "", uid: "", rl: "" }
        })
            .done(function( msg ) {
                alert( "Data Saved: " + msg );
            });*/
    </script>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <div class="logo">
            <img style="width:50px;" class="respImg" src="images/secondary-block.jpg">
        </div> <!-- END OF LOGO -->
        <div class="menu">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </div> <!-- END OF MENU -->
        <div class="clear"></div>
    </div> <!-- END OF HEADER -->
    <div class="legend">
        <div class="least">
            Least Drought Stricken
        </div>
        <div class="most">
            Most Drought Stricken
        </div>
        <div class="clear"></div>
    </div>
    <div class="mapCon">
        <div class="mapLeftCon">
            <div class="controlBox">
                <ul>
                    <?php
                        controlBox($uid, "l", $fetchR, $fetchIDR, $fetchIDL, $fetchL);
                    ?>
                </ul>
                <div class="selectBox" id="selectBox">
                    <form method="get">
                        <select name="mapIDL">
                            <?php
                                selectPop($fetchL, $fetchR, $fetchIDL, $fetchIDR, "l");
                            ?>
                        </select>
                        <input type="hidden" name="idL" value=<?php echo "$fetchL" ?>>
                        <input type="hidden" name="idR" value=<?php echo "$fetchR" ?>>
                        <input type="hidden" name="mapIDR" value=<?php echo "$fetchIDR" ?>>
                        <input type="hidden" name="mapIDL" value=<?php echo "$fetchIDL" ?>>
                        <input type="submit" value="Select" name="submit">
                    </form>
                </div> <!-- END OF SELECTBOX -->
            </div> <!-- END OF CONTROLBOX -->
            <div class="mapLeft">
                <?php
                    generateMap($uid, $fetchL, $fetchR, $fetchIDL, $fetchIDR, "l");
                    ?>
                <!-- Query and generation for left map here -->
            </div> <!-- END OF MAPLEFT -->
        </div> <!-- END OF MAPLEFTCON -->
        <div class="mapRightCon">
            <div class="controlBox">
                <ul>
                    <?php
                        controlBox($uid, "r", $fetchR, $fetchIDR, $fetchIDL, $fetchL);
                    ?>
                </ul>
                <div class="selectBox" id="selectBox">
                    <form method="get">
                        <select name="mapIDR">
                            <?php
                                selectPop($fetchL, $fetchR, $fetchIDL, $fetchIDR, "r");
                            ?>
                        </select>
                        <input type="hidden" name="idL" value=<?php echo "$fetchL" ?>>
                        <input type="hidden" name="idR" value=<?php echo "$fetchR" ?>>
                        <input type="hidden" name="mapIDR" value=<?php echo "$fetchIDR" ?>>
                        <input type="hidden" name="mapIDL" value=<?php echo "$fetchIDL" ?>>
                        <input type="submit" value="Select" name="submit">
                    </form>
                </div> <!-- END OF SELECTBOX -->
            </div> <!-- END OF CONTROLBOX -->
            <div class="mapRight">
                <?php
                    generateMap($uid, $fetchL, $fetchR, $fetchIDL, $fetchIDR, "r");
                ?>
                <!-- Query and generation for right map here -->
            </div> <!-- END OF MAPRIGHT -->
        </div> <!-- END OF MAPRIGHTCON -->
    </div> <!-- END OF MAPCON -->
</div> <!-- END OF WRAPPER-->
</body>
</html>
