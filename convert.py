from PIL import Image
import sys

inFile = sys.argv[1]
outFile = sys.argv[2]
img = Image.open(inFile)
img = img.convert("RGBA")

pixdata = img.load()

for y in xrange(img.size[1]):
    for x in xrange(img.size[0]):
        if pixdata[x, y] == (240, 240, 240, 255):
            pixdata[x, y] = (240, 240, 240, 0)

img.save(outFile, "PNG")
