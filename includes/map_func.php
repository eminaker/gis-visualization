<?php
/*
 * Homepage; shows the user two maps (by default map 1,1 and map 1,1). User
 * can use the divs in .controlBox to select different datasets, all
 * options pulled from the database. After selecting a database the page
 * will reload with new date options, the user may select a date from the
 * select element, the page will reload with the map updated.
 * Dataset and map selections are stored as query string vars.
 * UserID is kept with cookie.
 *
 * When images are generated, they're passed through a series of GDAL
 * commands to render the AAI/GRID files into color-relief .JPG files. The
 * files (and all intermediate temporary files) are stored in userdata/$uid
 * where $uid is a randomly generated number. After the commands are
 * executed, the image is sent to the user's computer as a data stream.
 *
 * Post-execution and display, all files are deleted from the server.
 */
//Cookie to identify user and session
function getUID(){
    if(!($_COOKIE["UID"])){
        $uid = rand(1000,9999);
        setcookie("UID", $uid, time()+3600);

        $mkCMD="mkdir userdata/$uid 2>> out.error";

        if((file_exists("userdata/$uid"))){
            $rmCMD="rm -rf userdata/$uid";
            exec($rmCMD, $errorout);

            $mkCMD="mkdir userdata/$uid 2>> out.error";
            exec($mkCMD, $errorout);
        }
        elseif(!(file_exists("userdata/$uid"))){
            $mkCMD="mkdir userdata/$uid 2>> out.error";
            exec($mkCMD, $errorout);
        }

    }
    else{
        $uid = $_COOKIE["UID"];
        if((file_exists("userdata/$uid"))){
            $rmCMD="rm -rf userdata/$uid";
            exec($rmCMD, $errorout);

            $mkCMD="mkdir userdata/$uid 2>> out.error";
            exec($mkCMD, $errorout);
        }
        elseif(!(file_exists("userdata/$uid"))){
            $mkCMD="mkdir userdata/$uid 2>> out.error";
            exec($mkCMD, $errorout);
        }
    }
    return($uid);
}
function controlBox($uid, $rl, $fetchR, $fetchIDR, $fetchIDL, $fetchL){
    if($rl == "r"){
        $rl = "mapIDR";
        $mapID = $fetchIDR;
        $map = $fetchR;
    }
    elseif($rl == "l"){
        $rl = "mapIDL";
        $mapID = $fetchIDL;
        $map = $fetchL;
    }
    else{
        die("Invalid map selection");
    }

    $userFilepath = "userdata/$uid";

    $query = 'SELECT name FROM datasets';
    $result = mysql_query($query) or die('Query failed, no datasets found: ' . mysql_error());

    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)){
        foreach ($line as $col_value){
            $queryI = "SELECT id FROM datasets WHERE name LIKE '$col_value'";
            $resultI = mysql_query($queryI) or die ('Query failed, dataset ID not found: ' . mysql_error());
            $datasetID = mysql_result($resultI, 0);

            if($datasetID == $map)
                $isActive="primary";
            else
                $isActive="default";

            echo "\t\t\t\t\t<a class=\"btn btn-$isActive btn-lg\" style=\"margin-right:10px;\" href=\"?idL=$datasetID&idR=$fetchR&mapIDR=$fetchIDR&mapIDL=$fetchIDL\"><li>$col_value</li></a>\n";
        }
    }
}

function selectPop($fetchL, $fetchR, $fetchIDR, $fetchIDL, $rl){
    if($rl == "r"){
        $rl = "mapIDR";
        $mapID = $fetchIDR;
        $map = $fetchR;
    }
    elseif($rl == "l"){
        $rl = "mapIDL";
        $mapID = $fetchIDL;
        $map = $fetchL;
    }
    else{
        die("Inalid map selection");
    }

    $query = "SELECT date FROM maps WHERE dataset LIKE '$map'";
    $result = mysql_query($query) or die('Query failed, no maps found for this dataset: ' . mysql_error());

    //echo "<select name=\"$rl\">\n";

    while($line = mysql_fetch_array($result, MYSQL_ASSOC)){
        foreach($line as $col_value){
            $queryI = "SELECT id FROM maps WHERE date LIKE '$col_value' AND dataset LIKE '$map'";
            $resultI = mysql_query($queryI) or die ('Query failed, map ID not found: ' . mysql_error());
            $finResult = mysql_result($resultI, 0);

            if($finResult == $mapID)
                $isSelected="selected=\"selected\"";
            else
                $isSelected="";

            echo "\t\t\t\t\t\t\t<option value='$finResult' $isSelected>$col_value</option>\n";
        }
    }

    //echo "\t\t\t\t\t\t</select>\n";
    /*echo "\t\t\t\t\t\t<input type=\"hidden\" name=\"idL\" value=\"$fetchL\">\n";
    echo "\t\t\t\t\t\t<input type=\"hidden\" name=\"idR\" value=\"$fetchR\">\n";
    echo "\t\t\t\t\t\t<input type=\"hidden\" name=\"mapIDR\" value=\"$fetchIDR\">\n";
    echo "\t\t\t\t\t\t<input type=\"hidden\" name=\"mapIDL\" value=\"$fetchIDL\">\n";
    echo "\t\t\t\t\t\t<input type=\"submit\" value=\"Select\" name=\"submit\">\n";*/
}

function generateMap($uid, $fetchL, $fetchR, $fetchIDL, $fetchIDR, $rl){
    /*Use combination of dataset ID and map ID (idR, mapIDR/idL, mapIDL) to query DB for mapData
    field - pass that data to gdal_translate/gdaldem using GDAL PHP bindings - store image file
    in isolated folder and put filename in cookie as generated left or right map.
    once new map is selected, delete the old map from that folder. Different folder for each concurrent
    user - decided by presence of cookie. Give session-based UID to each user*/
    if($rl == "r"){
        $rl = "mapIDR";
        $mapID = $fetchIDR;
        $map = $fetchR;
    }
    elseif($rl == "l"){
        $rl = "mapIDL";
        $mapID = $fetchIDL;
        $map = $fetchL;
    }
    else{
        die("Inalid map selection");
    }

    srand();

    $appRoot = "/var/www/gisVisualization";
    $userFilepath = "userdata/$uid";

    $query = "SELECT mapData FROM maps WHERE id LIKE '$mapID'";
    $result = mysql_query($query) or die('Query failed, no maps fould for this dataset/ID combintion: ' . mysql_error());
    $finResult = mysql_result($result, 0);

    $filename = rand(1000,9999);

    //Translate Esri grid into monochromatic tif
    $cmd = "gdal_translate -of GTiff -outsize 500% 500% $appRoot/$finResult $appRoot/$userFilepath/$filename.tif 2>> out.error";
    exec($cmd, $output);

    //Create a color-relief using the monochromatic tif
    $cmd = "gdaldem color-relief $appRoot/$userFilepath/$filename.tif $appRoot/419CNN.txt $appRoot/$userFilepath/temp$filename.tif 2>> out.error";
    exec($cmd, $output);

    //Convert color-relief from tif to PNG
    $cmd = "gdal_translate -of PNG $appRoot/$userFilepath/temp$filename.tif $appRoot/$userFilepath/afin$filename.png 2>> out.error";
    exec($cmd, $output);

    //Remove white background and reduce signal noise
    $cmd = "python convert.py $appRoot/$userFilepath/afin$filename.png $appRoot/$userFilepath/fin$filename.png 2>> out.error";
    exec($cmd, $output);

    //The following three commands remove temp image files
    $cmd = "rm $appRoot/$userFilepath/temp$filename.tif";
    exec($cmd);
    $cmd = "rm $appRoot/$userFilepath/$filename.tif";
    exec($cmd);
    $cmd = "rm $appRoot/$userFilepath/afin$filename.png";
    exec($cmd);

    $src=base64_encode(file_get_contents("$userFilepath/fin$filename.png"));
    echo "<img class=\"respImg shadow\" src=\"data:image/png;base64, $src\">";
}
?>
