<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';


?>
<html>
<head>
    <title>Visualizing Drought :: LSSU</title>
    <meta charset="utf-8">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts.css">
    <script src="js/jquery-3.0.0.min.js"></script>
    <script src="forms.js"></script>
    <script src="sha512.js"></script>
    <script type="text/javascript">
        $(function(){
            $('#close').click(function(){
                $('#infoBox').slideUp();
            });
        });
    </script>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <div class="logo">
            <img style="width:50px;" class="respImg" src="images/secondary-block.jpg">
        </div> <!-- END OF LOGO -->
        <div class="menu">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </div> <!-- END OF MENU -->
        <div class="clear"></div>
    </div> <!-- END OF HEADER -->
    <div class="contentCon">
    <h2>About the Project</h2>
    <p>This application was developed to assist the Lake Superior State University geology department in ongoing research on drought patterns. They were looking for a convenient method to process, catalog, and visualize hundreds of Esri Grid datasets containing rainfall statistics for North America. While local software applications were considered, the final solution was a web application. It would contain an administration area for uploading and managing the datasets and a public homepage where users could view the datasets.</p>
    <p>PHP forms the base of the application, powering the administrative section, the login and authentication, and serving as middleware between the application and both GDAL and Python scripts used for image processing.</p>
    <p><a href="http://www.gdal.org/">GDAL</a> is used for processing the textual datasets into monochromatic images. These black and white images are then passed through two more GDAL provided functions to create a color-relief and then convert and resize the image for display. A Python script, making heavy use the Python Imaging Library, was written to remove noise from the final render and convert the white background to a transparent background. The map is finally sent to the user as a base64-encoded image and all of the intermediate files are removed from the server.</p>
    <p>The project was planned and executed largely thanks to the cooperation of Dr. Andrea Munoz-Hernandez, the Lake Superior State University School of Physical Sciences, Dr. Christopher Smith, and the Lake Superior State University School of Mathematics and Computer Science.</p>
    </div> <!-- END OF CONTENTCON -->
</div> <!-- END OF WRAPPER -->
</body>
</html>
